package cz.spiffyk.hexaskvorky.android.ui

import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cz.spiffyk.hexaskvorky.core.HexaskvorkyGame
import cz.spiffyk.hexaskvorky.core.HexaskvorkyGameLogic

/**
 * Model containing data for the currently played game in [GameActivity].
 *
 * @param numPlayers
 *      number of players
 * @param endScore
 *      length of the winning row of hexagons
 */
class GameViewModel private constructor(numPlayers: Int, endScore: Int) : ViewModel() {

    companion object {
        val colorFilters = arrayOf(
            PorterDuffColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY),
            PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY),
            PorterDuffColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY),
            PorterDuffColorFilter(Color.CYAN, PorterDuff.Mode.MULTIPLY),
            PorterDuffColorFilter(Color.MAGENTA, PorterDuff.Mode.MULTIPLY),
            PorterDuffColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY)
        )

        const val NUM_PLAYERS_PARAM = "numPlayers"
        const val END_SCORE_PARAM = "endScore"

        const val DEFAULT_NUM_PLAYERS = 2
        const val DEFAULT_END_SCORE = 5
    }

    /**
     * The platform agnostic game logic object.
     */
    val game: HexaskvorkyGame = HexaskvorkyGameLogic(numPlayers, endScore)

    /**
     * Color filters for players' hexagon coloring.
     */
    val playerColors: List<ColorFilter> = (0 until game.numPlayers).asSequence()
        .map { playerNum -> colorFilters[playerNum % game.numPlayers] }
        .toList()

    /**
     * X-coordinate offset for view scrolling.
     */
    var viewOffsetX: Float = 0f

    /**
     * Y-coordinate offset for view scrolling.
     */
    var viewOffsetY: Float = 0f

    /**
     * Scale for view zoom.
     */
    var scale: Float = 1f

    /**
     * [ViewModel] factory for parametrized creation of [GameViewModel]s.
     */
    @Suppress("UNCHECKED_CAST")
    class Factory(private val numPlayers: Int, private val endScore: Int) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (!GameViewModel::class.java.isAssignableFrom(modelClass)) {
                throw IllegalArgumentException("Unsupported model class $modelClass")
            }

            return GameViewModel(numPlayers, endScore) as T
        }
    }
}