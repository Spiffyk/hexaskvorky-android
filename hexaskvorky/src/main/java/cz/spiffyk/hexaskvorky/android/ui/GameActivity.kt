package cz.spiffyk.hexaskvorky.android.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import cz.spiffyk.hexaskvorky.core.HexaskvorkyGame
import cz.spiffyk.hexaskvorky.R

/**
 * Activity containing the [HexaskvorkyView] for playing the game.
 */
class GameActivity : AppCompatActivity() {

    private val model: GameViewModel by viewModels {
        val numPlayers: Int = intent.extras?.getInt(GameViewModel.NUM_PLAYERS_PARAM, GameViewModel.DEFAULT_NUM_PLAYERS)
            ?: GameViewModel.DEFAULT_NUM_PLAYERS
        val endScore: Int = intent.extras?.getInt(GameViewModel.END_SCORE_PARAM, GameViewModel.DEFAULT_END_SCORE)
            ?: GameViewModel.DEFAULT_END_SCORE

        return@viewModels GameViewModel.Factory(numPlayers, endScore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        val toolbar: Toolbar = findViewById(R.id.gameToolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val gameView: HexaskvorkyView = findViewById(R.id.hexaskvorkyView)
        gameView.model = model
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_game, menu)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.recenterAction -> {
                findViewById<HexaskvorkyView>(R.id.hexaskvorkyView).recenter()
                return true
            }
        }

        return false
    }

    override fun onBackPressed() {
        if (model.game.state == HexaskvorkyGame.State.FINISHED) {
            finish()
            return
        }

        AlertDialog.Builder(this)
            .setTitle(R.string.exit_confirmation_title)
            .setPositiveButton(R.string.exit_yes) { _, _ -> finish() }
            .setNegativeButton(R.string.exit_no, null)
            .show()
    }
}