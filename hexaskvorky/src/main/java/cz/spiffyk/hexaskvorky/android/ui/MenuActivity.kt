package cz.spiffyk.hexaskvorky.android.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import cz.spiffyk.hexaskvorky.android.online.HexaskvorkyOnlineClient
import cz.spiffyk.hexaskvorky.R

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
    }

    fun onLocalGameClicked(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, LocalLobbyActivity::class.java))
    }

    fun onOnlineGameClicked(view: View) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(view.context)
        val host = sharedPreferences.getString("onlineHost", null)
        val username = sharedPreferences.getString("onlineUser", null)
        val password = sharedPreferences.getString("onlinePass", null)

        if (host.isNullOrBlank() || username.isNullOrBlank() || password.isNullOrBlank()) {
            Toast.makeText(view.context, "Set your online settings first!", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, SettingsActivity::class.java))
            return
        }

        val client = HexaskvorkyOnlineClient(host, username, password)
        client.login(this,
            { Toast.makeText(view.context, "Logged in!", Toast.LENGTH_SHORT).show() },
            { Toast.makeText(view.context, "Login failed: ${it.localizedMessage}", Toast.LENGTH_SHORT).show() })
    }

    fun onSettingsClicked(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, SettingsActivity::class.java))
    }
}