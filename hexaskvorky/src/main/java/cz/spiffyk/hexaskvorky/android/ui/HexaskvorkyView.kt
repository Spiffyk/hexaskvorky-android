package cz.spiffyk.hexaskvorky.android.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BlurMaskFilter
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.core.content.res.getDimensionOrThrow
import androidx.core.content.res.getDrawableOrThrow
import androidx.core.graphics.applyCanvas
import androidx.core.graphics.drawable.toDrawable
import androidx.core.math.MathUtils
import androidx.preference.PreferenceManager
import cz.spiffyk.hexaskvorky.core.HexaskvorkyGame
import cz.spiffyk.hexaskvorky.core.vec.Point2i
import cz.spiffyk.hexaskvorky.core.vec.dist
import cz.spiffyk.hexaskvorky.core.vec.sqDist
import cz.spiffyk.hexaskvorky.R
import kotlin.math.max
import kotlin.math.min

/**
 * A view for rendering and controlling an instance of [HexaskvorkyGame].
 */
class HexaskvorkyView : View {

    companion object {
        const val HEXAGON_MAX_RESOLUTION = 512
        const val HEXAGON_MIN_RESOLUTION = 16
        const val UNSTICK_DIST = 50f
        const val HEX_TEXT_SIZE = 40f
        const val MIN_SCALE = 0.5f
        const val MAX_SCALE = 4f
    }

    /**
     * Enumeration for the state of the view's touch controls.
     */
    private enum class InputState {
        PLAY, MOVE
    }

    /** Currently "hovered" point. */
    private var activePoint: Point2i? = null

    // Fields for touch controls
    private var inputState: InputState = InputState.PLAY
    private var lastTouchX: Float? = null
    private var lastTouchY: Float? = null
    private var lastTouchDist: Float? = null
    private var lastTouchCount: Int = 0

    // Persistent helper variables
    private var helperRect = Rect()
    private var helperCoords = MotionEvent.PointerCoords()

    /** Cache for the hexagon graphic */
    private var hexagonCache: BitmapDrawable? = null

    private var _model: GameViewModel? = null

    private var stateTextPaint = TextPaint().apply {
        color = Color.WHITE
        textSize = 30f
    }

    private var hexTextPaint = TextPaint().apply {
        typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        color = Color.argb(80, 255, 255, 255)
    }

    private var playerIndicatorPaint = Paint().apply {
        color = Color.WHITE
        style = Paint.Style.STROKE
        strokeWidth = 20f
    }

    private var playerIndicatorShadowPaint = Paint().apply {
        color = Color.BLACK
        style = Paint.Style.STROKE
        maskFilter = BlurMaskFilter(10f, BlurMaskFilter.Blur.NORMAL)
        strokeWidth = 20f
    }

    private var _hexagonDiameter: Float = 30f
    private var _hexagonDrawable: Drawable? = null
    private var _hexagonTickDrawable: Drawable? = null
    private var _hexagonActivityMark: Drawable? = null
    private var _hexagonWinnerMark: Drawable? = null

    /**
     * ViewModel containing the game's logic and data.
     */
    var model: GameViewModel?
        get() = _model
        set(value) {
            _model = value
            invalidateGameBoard()
        }

    /**
     * The size diameter of a hexagon in dp.
     */
    var hexagonDiameter: Float
        get() = _hexagonDiameter
        set(value) {
            _hexagonDiameter = value
            invalidateGameBoard()
        }

    /**
     * The hexagon graphic.
     */
    var hexagonDrawable: Drawable?
        get() = _hexagonDrawable
        set(value) {
            _hexagonDrawable = value
            invalidateGameBoard()
        }

    /**
     * The hexagon tick graphic.
     */
    var hexagonTickDrawable: Drawable?
        get() = _hexagonTickDrawable
        set(value) {
            _hexagonTickDrawable = value
            invalidateGameBoard()
        }

    /**
     * The mark of a currently "hovered" hexagon.
     */
    var hexagonActivityMark: Drawable?
        get() = _hexagonActivityMark
        set(value) {
            _hexagonActivityMark = value
            invalidateGameBoard()
        }

    /**
     * The mark for hexagons that contributed to the winning state.
     */
    var hexagonWinnerMark: Drawable?
        get() = _hexagonWinnerMark
        set(value) {
            _hexagonWinnerMark = value
            invalidateGameBoard()
        }


    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.HexaskvorkyView, defStyle, 0
        )

        _hexagonDiameter = a.getDimensionOrThrow(R.styleable.HexaskvorkyView_hexagonDiameter)
        _hexagonDrawable = a.getDrawableOrThrow(R.styleable.HexaskvorkyView_hexagonDrawable)
        _hexagonTickDrawable = a.getDrawableOrThrow(R.styleable.HexaskvorkyView_hexagonTickDrawable)
        _hexagonActivityMark = a.getDrawableOrThrow(R.styleable.HexaskvorkyView_hexagonActivityMark)
        _hexagonWinnerMark = a.getDrawableOrThrow(R.styleable.HexaskvorkyView_hexagonWinnerMark)

        a.recycle()

        // Update TextPaint and text measurements from attributes
        invalidateGameBoard()
    }

    private fun invalidateGameBoard() {
        invalidate()
    }

    /**
     * Moves the view onto the center of all filled hexagons.
     */
    fun recenter() {
        _model?.let { model ->
            invalidate()

            var offsetX = 0f
            var offsetY = 0f
            val pointCount = model.game.filledHexesCount

            if (pointCount == 0) {
                model.viewOffsetX = 0f
                model.viewOffsetY = 0f
                return
            }

            model.game.forEachFilledHex { axialPoint, _ ->
                offsetX += Point2i.axialToXOffset(axialPoint.x, axialPoint.y)
                offsetY += Point2i.axialToYOffset(axialPoint.x, axialPoint.y)
            }
            model.viewOffsetX = (-offsetX * _hexagonDiameter) / pointCount
            model.viewOffsetY = (-offsetY * _hexagonDiameter) / pointCount
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null) {
            return false
        }

        this.invalidate()

        _model?.let { model ->
            val contentWidth = width - paddingLeft - paddingRight
            val contentHeight = height - paddingTop - paddingBottom

            var centerX = 0f
            var centerY = 0f
            for (i in 0 until event.pointerCount) {
                event.getPointerCoords(i, helperCoords)
                centerX += helperCoords.x
                centerY += helperCoords.y
            }
            centerX /= event.pointerCount
            centerY /= event.pointerCount

            if (lastTouchCount != event.pointerCount) {
                lastTouchX = centerX
                lastTouchY = centerY
                lastTouchDist = null
                lastTouchCount = event.pointerCount
            }

            val gameFieldOffsetX = screenToGame(centerX, contentWidth, model.viewOffsetX, model.scale)
            val gameFieldOffsetY = screenToGame(centerY, contentHeight, model.viewOffsetY, model.scale)
            val point = Point2i.offsetToAxial(gameFieldOffsetX - .5f, gameFieldOffsetY - .5f)

            when (inputState) {
                InputState.PLAY -> {
                    if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                        if (model.game.state == HexaskvorkyGame.State.IN_GAME) {
                            activePoint = point
                        }
                        lastTouchX = centerX
                        lastTouchY = centerY
                        lastTouchDist = touchDist(event)

                        return true
                    }

                    if (event.actionMasked == MotionEvent.ACTION_MOVE && lastTouchX != null && lastTouchY != null) {
                        if (event.pointerCount > 1 || sqDist(lastTouchX!!, lastTouchY!!, centerX, centerY) > (UNSTICK_DIST * UNSTICK_DIST)) {
                            activePoint = null
                            inputState =
                                InputState.MOVE
                            return true
                        }
                    }

                    if (event.actionMasked == MotionEvent.ACTION_UP) {
                        lastTouchX = null
                        lastTouchY = null
                        lastTouchDist = null
                        activePoint = null

                        if (model.game.state == HexaskvorkyGame.State.IN_GAME) {
                            model.game.play(point)

                            if (model.game.state == HexaskvorkyGame.State.FINISHED) {
                                Toast.makeText(
                                    this.context,
                                    resources.getString(R.string.winner_announcement, model.game.winner + 1),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                }

                InputState.MOVE -> {
                    activePoint = null

                    if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                        // should not happen but whatever
                        lastTouchX = centerX
                        lastTouchY = centerY
                        lastTouchDist = touchDist(event)
                    }

                    if (event.actionMasked == MotionEvent.ACTION_MOVE && lastTouchX !== null && lastTouchY !== null) {
                        touchDist(event)?.let { touchDist ->
                            lastTouchDist?.let { last ->
                                model.scale = MathUtils.clamp(model.scale * (touchDist / last), MIN_SCALE, MAX_SCALE)
                            }
                            lastTouchDist = touchDist
                        }

                        model.viewOffsetX += (centerX - lastTouchX!!) / model.scale
                        model.viewOffsetY += (centerY - lastTouchY!!) / model.scale

                        lastTouchX = centerX
                        lastTouchY = centerY
                    }

                    if (event.actionMasked == MotionEvent.ACTION_UP) {
                        inputState = InputState.PLAY
                        lastTouchX = null
                        lastTouchY = null
                        lastTouchDist = null
                    }
                }
            }
        }

        return true
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        _model?.let { model ->
            // cache the hexagon graphic
            val diameter =
                max(min((_hexagonDiameter * model.scale).toInt(), HEXAGON_MAX_RESOLUTION), HEXAGON_MIN_RESOLUTION)
            if (hexagonCache === null || hexagonCache?.bitmap?.width != diameter) {
                hexagonCache = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888).applyCanvas {
                    _hexagonDrawable?.let {
                        it.setBounds(0,  0, diameter, diameter)
                        it.draw(this)
                    }
                }.toDrawable(resources)
            }

            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.context)
            val debugVisualsEnabled = sharedPreferences.getBoolean("debugVisuals", false)

            val contentWidth = width - paddingLeft - paddingRight
            val contentHeight = height - paddingTop - paddingBottom

            // Calculate axial coordinate bounds for rendering - so that off-screen hexagons are not rendered
            val leftXOffset = screenToGame(0f, contentWidth, model.viewOffsetX, model.scale) - 1
            val topYOffset = screenToGame(0f, contentHeight, model.viewOffsetY, model.scale) - 1
            val rightXOffset = screenToGame(contentWidth.toFloat(), contentWidth, model.viewOffsetX, model.scale) + 1
            val bottomYOffset = screenToGame(contentHeight.toFloat(), contentHeight, model.viewOffsetY, model.scale) + 1
            val tlyAxial = Point2i.offsetToYAxial(leftXOffset, topYOffset)
            val trxAxial = Point2i.offsetToXAxial(rightXOffset, topYOffset)
            val blxAxial = Point2i.offsetToXAxial(leftXOffset, bottomYOffset)
            val bryAxial = Point2i.offsetToYAxial(rightXOffset, bottomYOffset)

            for (x in blxAxial .. trxAxial) {
                for (y in tlyAxial .. bryAxial) {
                    val currentAxialPoint = Point2i.of(x, y)

                    val topLeftX = gameToScreen(Point2i.axialToXOffset(x, y), contentWidth, model.viewOffsetX, model.scale)
                    val topLeftY = gameToScreen(Point2i.axialToYOffset(x, y), contentHeight, model.viewOffsetY, model.scale)
                    val bottomRightX = topLeftX + (_hexagonDiameter * model.scale)
                    val bottomRightY = topLeftY + (_hexagonDiameter * model.scale)

                    if (topLeftX > contentWidth || topLeftY > contentHeight || bottomRightX < 0 || bottomRightY < 0) {
                        // discard hexagon as it's off-screen
                        continue
                    }

                    // render hexagon background
                    hexagonCache?.let {
                        it.setBounds(
                            topLeftX.toInt(),
                            topLeftY.toInt(),
                            bottomRightX.toInt(),
                            bottomRightY.toInt()
                        )
                        it.draw(canvas)
                    }

                    // render hexagon tick
                    val player = model.game.valueAt(currentAxialPoint)
                    if (player !== null) {
                        _hexagonTickDrawable!!.let {
                            it.colorFilter = model.playerColors[player]
                            it.setBounds(
                                topLeftX.toInt(),
                                topLeftY.toInt(),
                                bottomRightX.toInt(),
                                bottomRightY.toInt()
                            )
                            it.draw(canvas)
                        }
                    }

                    // mark hexagon as "hovered"
                    if (activePoint == currentAxialPoint) {
                        _hexagonActivityMark!!.let {
                            it.colorFilter = model.game.currentPlayer.let(model.playerColors::get)
                            it.setBounds(
                                topLeftX.toInt(),
                                topLeftY.toInt(),
                                bottomRightX.toInt(),
                                bottomRightY.toInt()
                            )
                            it.draw(canvas)
                        }
                    }

                    // mark hexagon as contributing to the winning state
                    if (model.game.state == HexaskvorkyGame.State.FINISHED && model.game.winningPoints.contains(currentAxialPoint)) {
                        _hexagonWinnerMark!!.let {
                            it.setBounds(
                                topLeftX.toInt(),
                                topLeftY.toInt(),
                                bottomRightX.toInt(),
                                bottomRightY.toInt()
                            )
                            it.draw(canvas)
                        }
                    }

                    // show axial coordinates of the hexagon
                    if (debugVisualsEnabled) {
                        val centerX = (topLeftX + bottomRightX) / 2
                        val centerY = (topLeftY + bottomRightY) / 2

                        "${currentAxialPoint.x}\u2003${currentAxialPoint.y}".let {
                            hexTextPaint.textSize = HEX_TEXT_SIZE * model.scale
                            hexTextPaint.getTextBounds(it, 0, it.length, helperRect)
                            canvas.drawText(
                                it,
                                centerX - helperRect.width() / 2,
                                centerY + helperRect.height() / 2,
                                hexTextPaint
                            )
                        }
                    }
                }
            }

            canvas.drawRect(0f, 0f, contentWidth.toFloat(), contentHeight.toFloat(), playerIndicatorShadowPaint)
            canvas.drawRect(0f, 0f, contentWidth.toFloat(), contentHeight.toFloat(), playerIndicatorPaint.apply {
                colorFilter = model.playerColors[model.game.currentPlayer]
            })

            if (debugVisualsEnabled) {
                "inputState = $inputState".let {
                    canvas.drawText(it, 30f, 30f - stateTextPaint.ascent(), stateTextPaint)
                }
            }
        }
    }

    /**
     * Converts the game's offset coordinates to the view's screen coordinates.
     */
    private fun gameToScreen(v: Float, contentSize: Int, viewOffset: Float, scale: Float): Float =
        (viewOffset * scale) + v * (_hexagonDiameter * scale) + ((contentSize - (_hexagonDiameter * scale)) / 2)

    /**
     * Converts the view's screen coordinates to the game's offset coordinates.
     */
    private fun screenToGame(v: Float, contentSize: Int, viewOffset: Float, scale: Float): Float =
        (v - (viewOffset * scale) - ((contentSize - (_hexagonDiameter * scale)) / 2)) / (_hexagonDiameter * scale)

    /**
     * Calculates the distance between two touch points (if any).
     */
    private fun touchDist(event: MotionEvent): Float? {
        if (event.pointerCount != 2) {
            return null
        }

        event.getPointerCoords(0, helperCoords)
        val x1 = helperCoords.x
        val y1 = helperCoords.y
        event.getPointerCoords(1, helperCoords)
        val x2 = helperCoords.x
        val y2 = helperCoords.y

        return dist(x1, y1, x2, y2)
    }
}