package cz.spiffyk.hexaskvorky.android.ui

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import cz.spiffyk.hexaskvorky.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }
}