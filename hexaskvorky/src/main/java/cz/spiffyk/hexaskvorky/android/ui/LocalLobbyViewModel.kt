package cz.spiffyk.hexaskvorky.android.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Model containing user-settable settings of the game.
 */
class LocalLobbyViewModel : ViewModel() {

    val numPlayers: MutableLiveData<Int> = MutableLiveData(2)
    val endScore: MutableLiveData<Int> = MutableLiveData(5)

}