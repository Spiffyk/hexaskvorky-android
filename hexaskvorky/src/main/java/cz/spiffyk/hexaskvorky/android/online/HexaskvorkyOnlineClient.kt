package cz.spiffyk.hexaskvorky.android.online

import android.app.Activity
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.Call
import okhttp3.Callback
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import java.io.IOException
import kotlin.reflect.KClass

typealias FailureCb = (IOException) -> Unit

/** Facilitates communication with the Hexaskvorky server. */
class HexaskvorkyOnlineClient(
    host_arg: String,
    username_arg: String,
    password_arg: String
) {
    companion object {
        private val MEDIA_TYPE_JSON = "application/json; charset=utf-8".toMediaType()

    }

    val http: OkHttpClient = OkHttpClient.Builder()
        .protocols(listOf(Protocol.HTTP_1_1, Protocol.HTTP_2))
        .build()
    val mapper = jacksonObjectMapper()
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

    val host: String
    val username: String
    val password: String
    var token: String? = null

    init {
        require(host_arg.isNotBlank())
        require(username_arg.isNotBlank())
        require(password_arg.isNotBlank())

        host = host_arg.trim().removeSuffix("/")

        username = username_arg.trim()
        password = password_arg.trim()
    }

    private fun <T: Any> call(activity: Activity?, method: String, endpoint: String, respClass: KClass<T>,
                              data: Any? = null, onResponseCb: (T?) -> Unit = {}, onFailureCb: FailureCb) {
        val body = data?.let(mapper::writeValueAsString)
        val req = Request.Builder()
            .url("$host/${endpoint.trim().removePrefix("/")}")
            .apply { token?.let { header("Authorization:", "Bearer $token") } }
            .method(method, body?.toRequestBody(MEDIA_TYPE_JSON))
            .build()

        http.newCall(req).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                if (activity == null) {
                    onFailureCb(e)
                } else {
                    activity.runOnUiThread { onFailureCb(e) }
                }
            }

            override fun onResponse(call: Call, response: Response) {
                // TODO: separate I/O failure and HTTP status code failure
                val respObj = response.body?.string()?.let{ mapper.readValue(it, respClass.java) }
                if (activity == null) {
                    onResponseCb(respObj)
                } else {
                    activity.runOnUiThread { onResponseCb(respObj) }
                }
            }
        })
    }

    data class LoginDto(val email: String?, val password: String?)
    data class LoginOutDto(val token: String?)

    fun login(activity: Activity?, onResponseCb: (String?) -> Unit = {}, onFailureCb: FailureCb = {}) {
        call(activity, "POST", "user/login", LoginOutDto::class, LoginDto(username, password), {
            token = it?.token
            onResponseCb(token)
        }, onFailureCb)
    }
}