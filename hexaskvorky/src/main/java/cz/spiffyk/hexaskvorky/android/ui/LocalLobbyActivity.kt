package cz.spiffyk.hexaskvorky.android.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import cz.spiffyk.hexaskvorky.R

/**
 * Activity containing settings for a match.
 */
class LocalLobbyActivity : AppCompatActivity() {

    companion object {
        const val MIN_NUM_PLAYERS = 1
        const val MIN_END_SCORE = 3
    }

    private val model: LocalLobbyViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local_lobby)

        setSupportActionBar(findViewById(R.id.lobbyToolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupNumPlayers()
        setupEndScore()
    }

    /**
     * Prepares the setting of the number of players.
     */
    private fun setupNumPlayers() {
        val numPlayersIndicator: TextView = findViewById(R.id.numPlayersIndicator)
        val numPlayersSeekBar: SeekBar = findViewById(R.id.numPlayersSeekBar)

        numPlayersSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                model.numPlayers.postValue(seekBar?.progress!! + MIN_NUM_PLAYERS)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })

        model.numPlayers.observe(this) {
            numPlayersIndicator.text = it.toString()
            numPlayersSeekBar.progress = it - MIN_NUM_PLAYERS
        }
    }

    /**
     * Prepares the setting of end score.
     */
    private fun setupEndScore() {
        val endScoreIndicator: TextView = findViewById(R.id.endScoreIndicator)
        val endScoreSeekBar: SeekBar = findViewById(R.id.endScoreSeekBar)

        endScoreSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                model.endScore.postValue(seekBar?.progress!! + MIN_END_SCORE)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })

        model.endScore.observe(this) {
            endScoreIndicator.text = it.toString()
            endScoreSeekBar.progress = it - MIN_END_SCORE
        }
    }

    /**
     * Starts the game with the user-provided settings.
     */
    fun onStartClicked(@Suppress("UNUSED_PARAMETER") view: View) {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra(GameViewModel.NUM_PLAYERS_PARAM, model.numPlayers.value)
        intent.putExtra(GameViewModel.END_SCORE_PARAM, model.endScore.value)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return false
    }
}